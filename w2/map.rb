require 'ruby2d'
require("xmlsimple")

def find_y(lat)
    
    return(936063.62 - 49772.90 * lat.to_f) #-49,772.90x + 936,063.62
end

def find_x(lng)
   
    return(-4632589.91 + 46785.93 * lng.to_f) #46,785.93x - 4,632,589.91 46,785.93x - 4,632,589.91
    
end

@img = Image.new("maphome3.png", x:0, y:0)

set(width: 827, height: 621, background: "blue")

hash = XmlSimple.xml_in('26_ม.ค._2021_15_22_32.gpx')
tracks = hash['trk'][0]['trkseg'][0]['trkpt']

 
tracks.each do |track|
    # puts track
    Circle.new(x:find_x(track['lon']), y:find_y(track['lat']), radius: 5, color: "blue")
    # puts find_x(track['lng'])
  end
#   find_x(track['lng'])
show